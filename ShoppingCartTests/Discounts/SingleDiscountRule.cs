﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShopingCart;
using ShopingCart.Discounts;

namespace ShoppingCartTests.Discounts
{
    internal class SingleDiscountRule : DiscountRule
    {
        protected override bool AllowedInCart(ShoppingCart cart)
        {
            return !cart.Discounts.Any();
        }
    }
}
