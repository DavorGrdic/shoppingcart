﻿using System;
using ShopingCart;
using ShopingCart.Models;
using ShoppingCartTests.Discounts;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ShoppingCartTests
{
    [TestClass]
    public class ShoppingCartTests
    {
        #region Test data
        Product butter = new Product { SKU = "1", Name = "Butter", Price = 0.8M };
        Product milk = new Product { SKU = "2", Name = "Milk", Price = 1.15M };
        Product bread = new Product { SKU = "3", Name = "Bread", Price = 1M };
        #endregion

        [TestMethod]
        public void TestAddMultipleProducts()
        {
            ShoppingCart cart = new ShoppingCart(new SingleDiscountRule());
            cart.Add(butter);
            cart.Add(milk);
            cart.Add(bread);
            cart.Add(butter);
        }
    }
}
