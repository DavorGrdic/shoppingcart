﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopingCart.Models
{
    internal class CartItem
    {
        public CartItem(IProduct product) : this(product, 1)
        {

        }

        public CartItem(IProduct product, int quantity)
        {
            Product = product;
            Quantity = quantity;
        }

        public IProduct Product { get; private set; }
        public int Quantity { get; set; }
        public decimal Discount { get; set; }
        public decimal Subtotal
        {
            get
            {
                return Quantity * Product.Price - Discount;
            }
        }
    }
}
