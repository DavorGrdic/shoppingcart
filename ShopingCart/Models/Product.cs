﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopingCart.Models
{
    public class Product : IProduct
    {
        public string SKU { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
