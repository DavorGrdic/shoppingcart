﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopingCart.Models
{
    class OrderItem
    {
        public IProduct Product { get; set; }
        public decimal Discount { get; set; }
    }
}
