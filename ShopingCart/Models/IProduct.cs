﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopingCart.Models
{
    public interface IProduct
    {
        string SKU { get; }
        string Name { get; }
        decimal Price { get; }
    }
}
