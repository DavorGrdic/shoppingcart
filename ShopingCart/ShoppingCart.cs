﻿using ShopingCart.Discounts;
using ShopingCart.Models;
using ShopingCart.Output;
using System;
using System.Linq;
using System.Collections.Generic;

namespace ShopingCart
{
    public class ShoppingCart
    {
        #region Fields
        private ICollection<CartItem> items = new List<CartItem>();
        private ICollection<IDiscount> discounts = new List<IDiscount>();
        #endregion

        #region Properties
        public DiscountRule DiscountRule { get; private set; }
        public IReadOnlyCollection<IDiscount> Discounts
        {
            get
            {
                return discounts.ToList().AsReadOnly();
            }
        }
        #endregion

        #region Constructors
        public ShoppingCart(DiscountRule discountRules)
        {
            DiscountRule = discountRules ?? throw new ArgumentNullException();
        }
        #endregion

        #region Methods

        public ShoppingCart Add(IProduct product)
        {
            CartItem item = items.Where(i => i.Product.SKU == product.SKU).FirstOrDefault();
            if (item != null)
            {
                item.Quantity++;
            }
            else
            {
                items.Add(new CartItem(product, 1));
            }
            return this;
        }

        public ShoppingCart Add(IDiscount discount)
        {
            if (DiscountRule.Allows(discount).In(this))
            {
                discounts.Add(discount);
            }
            return this;
        }

        public void Output(ICartFormatter formatter)
        {

        }

        public decimal GetTotal()
        {
            return 0;
        }
        #endregion
    }
}
