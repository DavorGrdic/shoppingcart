﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopingCart.Discounts
{
    public interface IDiscount
    {
        void Apply(ShoppingCart cart);
    }
}
