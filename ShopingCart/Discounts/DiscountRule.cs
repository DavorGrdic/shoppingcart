﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopingCart.Discounts
{
    public abstract class DiscountRule
    {
        protected IDiscount discount;

        public DiscountRule Allows(IDiscount discount)
        {
            this.discount = discount ?? throw new ArgumentNullException();
            return this;
        }

        protected abstract bool AllowedInCart(ShoppingCart cart);

        public bool In(ShoppingCart cart)
        {
            if (cart != null && discount != null)
            {
                return AllowedInCart(cart);
            }
            return false;
        }
    }
}
