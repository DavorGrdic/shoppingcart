﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopingCart.Discounts
{
    public interface IDiscountRule
    {
        IDiscountRule Allows(IDiscount discount);
        bool In(ShoppingCart cart);
    }
}
