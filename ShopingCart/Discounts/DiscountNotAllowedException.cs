﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopingCart.Discounts
{
    public class DiscountNotAllowedException : Exception
    {
        public DiscountNotAllowedException()
        {
        }

        public DiscountNotAllowedException(string message) : base(message)
        {
        }
    }
}
